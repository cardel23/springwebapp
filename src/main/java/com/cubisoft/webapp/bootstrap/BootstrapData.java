package com.cubisoft.webapp.bootstrap;

import com.cubisoft.webapp.domain.Author;
import com.cubisoft.webapp.domain.Book;
import com.cubisoft.webapp.domain.Publisher;
import com.cubisoft.webapp.repositories.AuthorRepository;
import com.cubisoft.webapp.repositories.BookRepository;
import com.cubisoft.webapp.repositories.PublisherRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class BootstrapData implements CommandLineRunner {

    private final AuthorRepository authorRepository;
    private final BookRepository bookRepository;
    private final PublisherRepository publisherRepository;

    public BootstrapData(AuthorRepository authorRepository, BookRepository bookRepository, PublisherRepository publisherRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
        this.publisherRepository = publisherRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        Publisher publisher = new Publisher("Editorial","olakase","masaya","42200");
        publisherRepository.save(publisher);

        Author author1 = new Author("Carlos", "Delgadillo");
        Book book1 = new Book("Ola", "K ase");
        book1.setPublisher(publisher);
        author1.getBooks().add(book1);
        book1.getAuthors().add(author1);

        authorRepository.save(author1);
        bookRepository.save(book1);

        Author author2 = new Author("Fer", "Porto");
        Book book2 = new Book("Libro","delDogg");
        author2.getBooks().add(book2);
        book2.getAuthors().add(author2);
        book2.setPublisher(publisher);
        authorRepository.save(author2);
        bookRepository.save(book2);

        publisher.getBooks().add(book1);
        publisher.getBooks().add(book2);
        publisherRepository.save(publisher);



        System.out.println("Started in boostrap");
        System.out.println("Libros: "+bookRepository.count());
        System.out.println("Autores: "+authorRepository.count());
        System.out.println("Editoriales: "+publisherRepository.count());
        System.out.println("Publicados: "+publisher.getBooks().size());


    }
}

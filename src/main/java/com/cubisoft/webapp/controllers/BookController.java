package com.cubisoft.webapp.controllers;

import com.cubisoft.webapp.repositories.BookRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by pwk04 on 11-21-20
 */
@Controller // Le dice a spring que esto es un controlador
public class BookController {

    private final BookRepository bookRepository;

    // Spring inyecta la clase bookRepository
    public BookController(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @RequestMapping("/books")
    public String getBooks(Model model){

        model.addAttribute("books", bookRepository.findAll());

        return "books/list";

    }
}

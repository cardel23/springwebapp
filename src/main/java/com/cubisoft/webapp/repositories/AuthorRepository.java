package com.cubisoft.webapp.repositories;

import com.cubisoft.webapp.domain.Author;
import org.springframework.data.repository.CrudRepository;

public interface AuthorRepository extends CrudRepository<Author, Long> {

}

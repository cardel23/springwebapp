package com.cubisoft.webapp.repositories;

import com.cubisoft.webapp.domain.Book;
import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<Book, Long> {
}

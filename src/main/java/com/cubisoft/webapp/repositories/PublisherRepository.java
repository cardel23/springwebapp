package com.cubisoft.webapp.repositories;

import com.cubisoft.webapp.domain.Publisher;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by pwk04 on 11-21-20
 */
public interface PublisherRepository extends CrudRepository<Publisher, Long> {
}
